###*
    The main domotix app.
    Loads dependencies and initializes the interface.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"

  ### Setting some globals ###
  global = global or window
  global.require = (if typeof require isnt "undefined" then require else false)

  if typeof define is "function" and define.amd
    ### Configure requirejs and load dependencies... ###  
    require.config
      baseUrl: "/assets/scripts"
      paths:
        ### Vendor libraries ###
        jslib: "../vendor/jquery/dist/jquery"
        bootstrap: "../vendor/bootstrap/dist/js/bootstrap"
        ### Control panels ###
        panel: "panels/panel"
        tempPanel: "panels/temp-panel"
        lightPanel: "panels/light-panel"
        curtainPanel: "panels/curtain-panel"
        ### Server emulation ###
        server: "server/server"
        component: "server/components/component"
        temp: "server/components/temp-component"
        light: "server/components/light-component"
        curtain: "server/components/curtain-component"
      map:
        "*":
          "jquery": "jslib"
      shim:
        jslib:
          exports: "$"
    require [ "jslib", "bootstrap", "server", "tempPanel", "lightPanel", "curtainPanel" ],
      plugin

  else
    ### ... or fall back to standard loading ###
    plugin @$, null, @server, @DomotixTempPanel, @DomotixLightPanel, @DomotixCurtainPanel

) domotixInit = ($, _, server, DomotixTempPanel, DomotixLightPanel, DomotixCurtainPanel) ->
  "use strict"

  ### Defining the class for the client ###
  class DomotixApp

    ###
        The default constructor

        - `s` [DomotixServer]: the server instance to connect to
    ###
    constructor: (s) ->
      @server = s
      @panels = []

      ### Generate the panel for each component ###
      panel = null
      Renderer = null
      for c in @server.getComponents()
        Renderer = DomotixApp.panelRenderers[c.getType()]
        if Renderer
          panel = new Renderer c, server
          @panels.push panel

    ### Get the panels for the components ###
    getPanels: () ->
      @panels

    ###
        Show the panels for the specified room.

        - `room` [String]: the room the components are in
    ###
    showPanels: (room) ->
      $("[data-room]").hide()
      $("[data-room=\"#{room}\"]").show()

    ###
        Trigger event for all components of a type.

        - `t` [String]: the component type
        - `e` [String]: the event to trigger
    ###
    triggerAll: (t, e) ->
      panels = @panels.filter( (p) -> p.component.getType() is t )
      cb = (c) ->
        p.updateComponent c
      for p in panels
        @server.triggerEvent p.component.getId(), e, cb

    ### Define renderers ###
    @panelRenderers =
      temperature: DomotixTempPanel
      curtain: DomotixCurtainPanel
      light: DomotixLightPanel



  ### Initializing a new client instance. ###
  client = new DomotixApp server

  ### Add rooms navigation ###
  section = $ "#content"
  navbar = $ ".navbar .navbar-nav"
  room = ""
  for r in server.getRooms()
    room = r.toLowerCase().replace /[^\w]/, "-"
    navbar.append $("<li><a href=\"##{room}\">#{r}</a></li>")
    section.append $("<section id=\"#{room}\" data-room=\"#{room}\" style=\"display:none\"><header><h1 class=\"page-header\">#{r}</h1></header><div id=\"#{room}-section\" class=\"row\"></div></section>")

  ### Handle rooms filtering ###
  navbar.on "click", "a", (e) =>
    window.location.hash = "#" + if e.target.href then e.target.href.split('#')[1] else ""
    $("li", navbar).removeClass "active"
    $(e.target.parentNode).addClass "active"
    client.showPanels if e.target.href then e.target.href.split('#')[1] else ""
    $(".navbar-collapse").collapse "hide"
  
  ### Pre-filter the room if specifed in the URL ###
  if window.location.hash
    hash = window.location.hash
    client.showPanels if hash then hash.substring(1) else ""
  
  ### Add the panels to the page ###
  section = null
  for p in client.getPanels()
    section = $ "##{p.component.getRoom().toLowerCase().replace /[^\w]/, "-"}-section"
    section.append p.getHTML()

  ### Handle global controllers ###
  $("#general-temperature-section").on "click", "[type=\"submit\"], .btn .icon", (e) =>
    e.preventDefault()
    e.stopPropagation()
    $target = $ e.target
    $target = $target.parent ".btn" if $target.is ".icon"
    op = $target.val()
    $("#general-temperature-section .btn").removeClass().addClass "btn btn-default"
    switch op
      when "day"
        client.triggerAll "temperature", "switchDayMode"
        $target.toggleClass "btn-default btn-success"
      when "night"
        client.triggerAll "temperature", "switchNightMode"
        $target.toggleClass "btn-default btn-primary"
      when "off"
        client.triggerAll "temperature", "switchOff"
        $target.toggleClass "btn-default btn-danger"
  $("#general-curtain-section").on "change", "input[type='checkbox']", (e, data) =>
    client.triggerAll "curtain", if e.target.checked then "open" else "close"
  $("#general-light-section").on "change", "input[type='checkbox']", (e, data) =>
    client.triggerAll "light", if e.target.checked then "switchOn" else "switchOff"

  client
