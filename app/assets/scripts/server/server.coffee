###*
    The server class emulating the actual server.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define "server", ["jslib", "component", "temp", "light", "curtain"], plugin
  else
    plugin @$, @DomotixComponent, @DomotixTempComponent, @DomotixLightComponent, @DomotixCurtainComponent
) domotixServer = ($, DomotixComponent, DomotixTempComponent, DomotixLightComponent, DomotixCurtainComponent) ->
  "use strict"

  ### Defining the class ###
  class DomotixServer

    ### Default constructor ###
    constructor: () ->
      @components = {}
      @rooms = []

    ###
        Add a new component

        - `c` [Object]: the component to add
    ###
    addComponent: (c) ->
      return console.err "invalid component type", c if not c instanceof DomotixComponent
      console.log "[init] Component: ", c
      @components[c.id] = c
      c

    ###
        Get a specific component
    
        - `id` [String]: the component id to look for
    ###
    getComponent: (id) ->
      @components[id]

    ### Get the components ###
    getComponents: () ->
      components = []
      for id, c of @components
        components.push c
      components

    ###
        Add a new room

        - `r` [String]: the room to add
    ###
    addRoom: (r) ->
      console.log "[init] Room: ", r
      @rooms.push r
      r

    ### Get the rooms ###
    getRooms: () ->
      @rooms

    ###
        Trigger an event for a specific component
    
        - `id` [String]: the component id
        - `e` [String]: the event to trigger
        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    triggerEvent: (id, e, cb) ->
      component = @components[id]
      console.log "[event]", id, e, component
      return cb null, "unknown component" if not component
      method = component[e]
      return cb null, "unknown event" if not method
      method.call component, cb



  ### initialize a new server instance ###
  server = new DomotixServer()
  
  ### Components for "Living room" ###
  living_room = "Living room"
  server.addRoom living_room
  server.addComponent new DomotixTempComponent("comp1", "Heater", living_room, { defaultTemp: 21, defaultStatus: "day" })
  server.addComponent new DomotixLightComponent("comp2", "Ceiling light", living_room)
  server.addComponent new DomotixLightComponent("comp3", "Couch lamp", living_room)
  server.addComponent new DomotixCurtainComponent("comp4", "Curtains", living_room)

  ### Components for "Bed room" ###
  bed_room = "Bed room"
  server.addRoom bed_room
  server.addComponent new DomotixTempComponent("comp5", "Heater", bed_room, { defaultTemp: 19, defaultStatus: "night" })
  server.addComponent new DomotixLightComponent("comp6", "Ceiling light", bed_room)
  server.addComponent new DomotixLightComponent("comp7", "Bedside lamp 1", bed_room, { defaultIntensity: 60 })
  server.addComponent new DomotixLightComponent("comp8", "Bedside lamp 2", bed_room, { defaultIntensity: 60 })
  server.addComponent new DomotixCurtainComponent("comp9", "Curtains", bed_room)
  
  ### Return the server instance ###
  server
