###*
    The curtain component class that represents the curtain controller.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define ["jslib", "component"], plugin
  else
    plugin @$, @DomotixComponent
) domotixCurtainComponent = ($, DomotixComponent) ->
  "use strict"

  ### Defining the class ###
  class DomotixCurtainComponent extends DomotixComponent

    ###
        Default constructor

        - `id` [String]: the component id
        - `name` [String]: the name that will be displayed for this component
        - `room` [String]: the room where the component is
        - `options` [Object]: options for the component (options.defaultStatus will define the default component status)
    ###
    constructor: (id, name = "", room, options) ->
      super id, name, room, "curtain", $.extend( { defaultStatus: "open" }, options )

    ###
        Set the component status after validation

        - `s` [String]: the new status
        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    setStatus: (s, cb) ->
      s = s.toLowerCase()
      return cb null, "invalid status" if not s.match /^(open|close)$/i
      super s, cb

    ###
        Set the component status to "open"

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    open: (cb) ->
      @setStatus "open", cb

    ###
        Set the component status to "close"

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    close: (cb) ->
      @setStatus "close", cb

  ### Return the class ###
  DomotixCurtainComponent
