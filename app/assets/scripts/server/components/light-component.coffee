###*
    The ligth component class that represents the light controller.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define ["jslib", "component"], plugin
  else
    plugin @$, @DomotixComponent
) domotixLightComponent = ($, DomotixComponent) ->
  "use strict"

  ### Defining the class ###
  class DomotixLightComponent extends DomotixComponent

    ###
        Default constructor

        - `id` [String]: the component id
        - `name` [String]: the name that will be displayed for this component
        - `room` [String]: the room where the component is
        - `options` [Object]: options for the component (options.defaultStatus will define the default component status)
    ###
    constructor: (id, name = "", room, options) ->
      super id, name, room, "light", $.extend( { defaultStatus: "off", defaultIntensity: 80, minIntensity: 10, maxIntensity: 100, intensityStep: 10 }, options )
      @intensity = @options.defaultIntensity

    ###
        Set the component status after validation

        - `s` [String]: the new status
        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    setStatus: (s, cb) ->
      s = s.toLowerCase()
      return cb null, "invalid status" if not s.match /^(on|off)$/i
      super s, cb

    ###
        Set the component status to "on"

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    switchOn: (cb) ->
      @setStatus "on", cb

    ###
        Set the component status to "off"

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    switchOff: (cb) ->
      @setStatus "off", cb

    ###
        Adjust the light intensity

        - `i` [int]: the new intensity (0 <= i <= 100)
        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    setIntensity: (i, cb) ->
      i = parseInt i, 10
      return cb null, "invalid intensity" if isNaN i
      return cb null, "invalid intensity range" if i < 0 or i > 100
      @intensity = i
      cb @

    ### Get the targeted light intensity ###
    getIntensity: () ->
      @intensity

    ### Get the current light intensity ###
    getCurrentIntensity: () ->
      ### Assume that intensity adjustments have an immediate effect ###
      @getIntensity()
    
    ###
        Increase the light intensity

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    increaseIntensity: (cb) ->
      @setIntensity Math.min(@intensity+@options.intensityStep, @options.maxIntensity), cb

    ###
        Decrease the light intensity

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    decreaseIntensity: (cb) ->
      @setIntensity Math.max(@intensity-@options.intensityStep, @options.minIntensity), cb

  ### Return the class ###
  DomotixLightComponent
