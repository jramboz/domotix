###*
    The component class provides the prototype for all other components.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define ["jslib"], plugin
  else
    plugin @$
) domotixComponent = ($) ->
  "use strict"

  ### Defining the class ###
  class DomotixComponent

    ###
        Default constructor
    
        - `id` [String]: the component id
        - `name` [String]: the name that will be displayed for this component
        - `room` [String]: the room where the component is
        - `type` [String]: the component type
        - `options` [Object]: options for the component (options.defaultStatus will define the default component status)
    ###
    constructor: (id, name = "", room, type, options) ->
      @id = id
      @name = name
      @room = room
      @type = type
      @options = options
      @status = options.defaultStatus

    ### Get the component id ###
    getId: () ->
      @id

    ### Get the component name ###
    getName: () ->
      @name

    ### Get the component room ###
    getRoom: () ->
      @room

    ### Get the component type ###
    getType: () ->
      @type

    ### Get the component status ###
    getStatus: () ->
      @status

    ###
        Set the component status

        - `s` [String]: the new status
        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    setStatus: (s, cb) ->
      @status = s
      cb @

  ### Return the class ###
  DomotixComponent

