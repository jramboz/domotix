###*
    The ligth component class that represents the light controller.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define ["jslib", "component"], plugin
  else
    plugin @$, @DomotixComponent
) domotixTempComponent = ($, DomotixComponent) ->
  "use strict"

  ### Defining the class ###
  class DomotixTempComponent extends DomotixComponent

    ###
        Default constructor

        - `id` [String]: the component id
        - `name` [String]: the name that will be displayed for this component
        - `room` [String]: the room where the component is
        - `options` [Object]: options for the component (options.defaultStatus will define the default component status)
    ###
    constructor: (id, name = "", room, options) ->
      super id, name, room, "temperature", $.extend( { defaultStatus: "off", defaultTemp: 21, minTemp: 14, maxTemp: 28, dayTemp: 21, nightTemp: 16 }, options )
      @temp = @options.defaultTemp

    ###
        Set the component status after validation

        - `s` [String]: the new status
        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    setStatus: (s, cb) ->
      s = s.toLowerCase()
      return cb null, "invalid status" if not s.match /^(day|night|off)$/i
      super s, cb

    ###
        Set the component status to "day"

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    switchDayMode: (cb) ->
      @setTemp @options.dayTemp, () =>
        @setStatus "day", cb

    ###
        Set the component status to "night"

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    switchNightMode: (cb) ->
      @setTemp @options.nightTemp, () =>
        @setStatus "night", cb

    ###
        Set the component status to "off"

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    switchOff: (cb) ->
      @setStatus "off", cb

    ###
        Adjust the desired temperature

        - `i` [int]: the new intensity (0 <= `i` <= 100)
        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    setTemp: (t, cb) ->
      t = parseInt t, 10
      return cb null, "invalid temperature" if isNaN t
      @temp = t
      cb @

    ### Get the desired temperature ###
    getTemp: () ->
      @temp

    ### Get the current temperature ###
    getCurrentTemp: () ->
      ### Assume the actual sensor is returning 19°C for this test case ###
      19

    ###
        Increase the temperature

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    increaseTemp: (cb) ->
      @setTemp Math.min(@temp+1, @options.maxTemp), cb

    ###
        Decrease the temperature

        - `cb` [Function]: the callback function to call (will get the updated component as parameter)
    ###
    decreaseTemp: (cb) ->
      @setTemp Math.max(@temp-1, @options.minTemp), cb

  ### Return the class ###
  DomotixTempComponent
