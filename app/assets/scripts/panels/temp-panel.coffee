###*
    The temperature panel class that renders the temperature control panel.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define ["jslib", "panel"], plugin
  else
    plugin @$, @DomotixPanel
) domotixTempPanel = ($, DomotixPanel) ->
  "use strict"

  ### Defining the class ###
  class DomotixTempPanel extends DomotixPanel

    ### Register event handlers ###
    handleEvents: () ->
      
      ### Handle clicks on buttons ###
      $(@body).on "click", "[type=\"submit\"], .btn .icon", (e) =>
        e.preventDefault()
        e.stopPropagation()
        $target = $ e.target
        $target = $target.parent ".btn" if $target.is ".icon"
        op = $target.val()
        reload = (c, err) =>
          return console.err err if err
          @updateComponent c
        switch op
          when "+" then @server.triggerEvent @component.id, "increaseTemp", reload
          when "–" then @server.triggerEvent @component.id, "decreaseTemp", reload
          when "day" then @server.triggerEvent @component.id, "switchDayMode", reload
          when "night" then @server.triggerEvent @component.id, "switchNightMode", reload
          when "off" then @server.triggerEvent @component.id, "switchOff", reload

    ### Fill the panel body for the specific component ###
    fillPanelBody: () ->
      
      ### The mode chooser ###
      $group = @buildButtonGroup().addClass "control-mode"
      $button = @buildButton($group)
        .attr
          title: "Enable day mode"
          value: "day"
        .html "<span class=\"icon icon-sun\"></span><span class=\"sr-only\">Day mode</span>"
      $button.toggleClass("btn-default btn-success") if @component.getStatus() is "day"
      $button = @buildButton($group)
        .attr
          title: "Enable night mode"
          value: "night"
        .html "<span class=\"icon icon-moon\"></span><span class=\"sr-only\">Night mode</span>"
      $button.toggleClass("btn-default btn-primary") if @component.getStatus() is "night"
      $button = @buildButton($group)
        .attr
          title: "Turn off"
          value: "off"
        .html "<span class=\"sr-only\">Switch</span>OFF"
      $button.toggleClass("btn-default btn-danger") if @component.getStatus() is "off"

      ### Add the current label ###
      @buildLabel().text "Current"
      
      ### The icon for the panel ###
      @buildIconPreview().text @component.getCurrentTemp() + "°"

      ### Add the adjust label ###
      @buildLabel().text "Adjust"
      
      ### The temperature adjuster ###
      $group = @buildButtonGroup().addClass "control-adjust"
      $button = @buildInputButton($group).attr
        title: "Decrease temperature"
        value: "–"
      $button.attr "disabled", "disabled" if @component.getTemp() <= @component.options.minTemp
      @buildLabelButton($group).text @component.getTemp() + "°"
      $button = @buildInputButton($group).attr
        title: "Increase temperature"
        value: "+"
      $button.attr "disabled", "disabled" if @component.getTemp() >= @component.options.maxTemp

      @body

  ### Return the class ###
  DomotixTempPanel
