###*
    The light panel class that renders the light control panel.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define ["jslib", "panel"], plugin
  else
    plugin @$, @DomotixPanel
) domotixLightPanel = ($, DomotixPanel) ->
  "use strict"

  ### Defining the class ###
  class DomotixLightPanel extends DomotixPanel

    ### Register event handlers ###
    handleEvents: () ->

      ### Handle clicks on switches ###
      $(@body).on "change", "input[type='checkbox']", (e, data) =>
        $target = $(e.target)
        checked = e.target.checked
        reload = (c, err) =>
          return console.err err if err
          @updateComponent c
        @server.triggerEvent @component.id, (if checked then "switchOn" else "switchOff"), reload

      ### Handle clicks on other buttons ###
      $(@body).on "click", "[type=\"submit\"], .btn .icon", (e) =>
        e.preventDefault()
        e.stopPropagation()
        $target = $(e.target)
        $target = $target.parent ".btn" if $target.is ".icon"
        op = $target.val()
        reload = (c, err) =>
          return console.err err if err
          @updateComponent c
        switch op
          when "+" then @server.triggerEvent @component.id, "increaseIntensity", reload
          when "–" then @server.triggerEvent @component.id, "decreaseIntensity", reload

    ### Fill the panel body for the specific component ###
    fillPanelBody: () ->

      ### The on/off switch ###
      @buildToggleSwitch("on").attr
        "data-checked-label": "on"
        "data-unchecked-label": "off"

      ### Add the current label ###
      @buildLabel().text "Current"
      
      ### The icon for the panel ###
      @buildIconPreview().text if @component.getStatus() is "on" then @component.getCurrentIntensity() + "%" else "0%"

      ### Add the adjust label ###
      @buildLabel().text "Adjust"
      
      ### The light intensity adjuster ###
      $group = @buildButtonGroup().addClass "control-adjust"
      $button = @buildInputButton($group).attr
        title: "Dim light"
        value: "–"
      $button.attr "disabled", "disabled" if @component.getIntensity() <= @component.options.minIntensity
      @buildLabelButton($group).text @component.getIntensity() + "%"
      $button = @buildInputButton($group).attr
        title: "Brighten light"
        value: "+"
      $button.attr "disabled", "disabled" if @component.getIntensity() >= @component.options.maxIntensity

      @body

  ### Return the class ###
  DomotixLightPanel
