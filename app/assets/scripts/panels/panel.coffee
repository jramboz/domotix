###*
    The panel class provides the prototype for all other panels.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define ["jslib"], plugin
  else
    plugin @$
) domotixPanel = ($) ->
  "use strict"

  ### Defining the class ###
  class DomotixPanel

    ###
        The default class constructor

        - `component` [Object]: the component for the panel
        - `server` [Object]: the server instance to forward the events to
    ###
    constructor: (component, server) ->
      @component = component
      @server = server
      
      @wrapper = $(document.createElement("div")).attr
        "data-room": @component.getRoom().toLowerCase().replace /[^\w]/, "-"
      @wrapper.addClass "col-micro col-xs-6 col-sm-4 col-md-3 col-lg-2 #{@component.getType()}-controller"

      $panel = $(document.createElement("section"))
      $panel.addClass "panel panel-default"
      @wrapper.append $panel
      
      $heading = $(document.createElement("header"))
      $heading.addClass "panel-heading"
      $panel.append $heading
      
      @title = $(document.createElement("h2"))
      @title.addClass "panel-title"
      @title.text @component.getName()
      $heading.append @title

      @body = $(document.createElement("div"))
      @body.addClass "panel-body status-" + @component.getStatus()
      $panel.append @body

      @body.append $(document.createElement("input")).attr
        type: "hidden"
        name: "component"
        value: @id

      @fillPanelBody()

      @handleEvents()

    ### Register event handlers ###
    handleEvents: () ->

    ### Handle button events ###
    handleEvents: (eventMap) ->
      $(@body).on "click", "[type=\"submit\"], .btn .icon", (e) =>
        e.preventDefault()
        e.stopPropagation()
        $target = $ e.target
        $target = $target.parent ".btn" if $target.is ".icon"
        op = $target.val()
        reload = (c, err) =>
          return console.err err if err
          @updateComponent c
        switch op
          when "+" then @server.triggerEvent @component.id, "increaseTemp", reload
          when "–" then @server.triggerEvent @component.id, "decreaseTemp", reload
          when "day" then @server.triggerEvent @component.id, "switchDayMode", reload
          when "night" then @server.triggerEvent @component.id, "switchNightMode", reload
          when "off" then @server.triggerEvent @component.id, "switchOff", reload

    ###
        Update the component

        - `c` [Object]: the new component for the panel
    ###
    updateComponent: (c) ->
      @component = c
      @updatePanel()
      @component

    ### Update the panel ###
    updatePanel: () ->
      @title.text @component.getName()
      @body.removeClass().addClass "panel-body status-" + @component.getStatus()
      @body.empty()
      @fillPanelBody()

    ### Fill the panel body for the specific component ###
    fillPanelBody: () ->
      @wrapper

    ### Get the HTML for the panel ###
    getHTML: () ->
      @wrapper

    ###
        Build the toggle swtich for the panel

        - `activeValue`: activate the switch if the component status has this value
    ###
    buildToggleSwitch: (activeValue) ->
      $toggleSwitch = $(document.createElement("input")).attr
        id: "#{@component.getId()}-switch"
        type: "checkbox"
      $toggleSwitch.attr "checked", "checked" if @component.getStatus() is activeValue
      @body.append $toggleSwitch
      $toggleLabel = $(document.createElement("label")).attr
        class: "btn btn-default"
        for: "#{@component.getId()}-switch"
        title: "Change status"
      $toggleLabel.html "<span class=\"sr-only\">Status</span>"
      @body.append $toggleLabel
      $toggleLabel

    ### Build a simple label for the panel ###
    buildLabel: () ->
      $label = $(document.createElement("div")).addClass "panel-label"
      @body.append $label
      $label

    ### Build the icon for the panel ###
    buildIconPreview: () ->
      $info = $(document.createElement("div")).addClass "well"
      @body.append $info
      $info

    ### Build a button group for the panel ###
    buildButtonGroup: () ->
      $group = $(document.createElement("div")).addClass "btn-group"
      @body.append $group
      $group

    ###
        Build an input button for the panel

        - `group` [Object]: the group the button belongs to
    ###
    buildInputButton: ($group) ->
      $button = $(document.createElement("input")).attr
        class: "btn btn-default"
        type: "submit"
      $group.append $button
      $button

    ###
        Build a button for the panel

        - `group` [Object]: the group the button belongs to
    ###
    buildButton: ($group) ->
      $button = $(document.createElement("button")).attr
        class: "btn btn-default"
        type: "submit"
      $group.append $button
      $button

    ###
        Build a label-button for the panel

        - `group` [Object]: the group the label belongs to
    ###
    buildLabelButton: ($group) ->
      $label = $(document.createElement("span")).addClass "btn btn-label"
      $group.append $label
      $label


  ### Return the class ###
  DomotixPanel

