###*
    The curtain panel class that renders the curtain control panel.
    
    **author** Julien Ramboz <julien.ramboz@gmail.com>
###

###
    AMD loader
    Try loading as AMD module or fall back to default loading
###
((plugin) ->
  "use strict"
  if typeof define is "function" and define.amd
    define ["jslib", "panel"], plugin
  else
    plugin @$, @DomotixPanel
) domotixCurtainPanel = ($, DomotixPanel) ->
  "use strict"

  ### Defining the class ###
  class DomotixCurtainPanel extends DomotixPanel

    ### Register event handlers ###
    handleEvents: () ->
      
      ### Handle clicks on switches ###
      $(@body).on "change", "input[type='checkbox']", (e, data) =>
        $target = $(e.target)
        checked = e.target.checked
        reload = (c, err) =>
          return console.err err if err
          @updateComponent c
        @server.triggerEvent @component.id, (if checked then "open" else "close"), reload
      
      ### Handle clicks on other buttons ###
      $(@body).on "click", "[type=\"submit\"], .btn .icon", (e) =>
        e.preventDefault()
        e.stopPropagation()
        $target = $(e.target)
        $target = $target.parent ".btn" if $target.is ".icon"
        op = $target.val()
        reload = (c, err) =>
          return console.err err if err
          @updateComponent c
        switch op
          when "+" then @server.triggerEvent @component.id, "increaseIntensity", reload
          when "–" then @server.triggerEvent @component.id, "decreaseIntensity", reload

    ### Fill the panel body for the specific component ###
    fillPanelBody: () ->

      ### The open/close switch ###
      @buildToggleSwitch("open").attr
        "data-checked-label": "open"
        "data-unchecked-label": "closed"

      ### Add the current label ###
      @buildLabel().text "Current"

      ### The icon for the panel ###
      @buildIconPreview().addClass if @component.getStatus() is "open" then "open" else "closed"

      @body

  ### Return the class ###
  DomotixCurtainPanel
