Domotix – Home automation web client
====================================

Domotix is a simple home automation web client built using [Bootstrap].  
Demo: [http://julienramboz.bitbucket.org/domotix/](http://julienramboz.bitbucket.org/domotix/)

Usage
-----

Just open `dist/index.html` in any recent browser, and enjoy.


### Light control panel ###

The light control panel allows to control individual light components.

The control panel is composed of:

- an "ON"/"OFF" toggle that enables/disables the light
- a pictogram indicating if the light is "ON" (yellow) or "OFF" (black), and the current brightness level the light is set to: 0% when off, otherwise the value specifed by the user
- a "-"/"+" brightness adjuster with the desired value


### Curtain control panel ###

The curtain control panel allows to control individual curtain components.

The control panel is composed of:

- an "OPEN"/"CLOSED" toggle that opens/closes the curtains
- a pictogram indicating if the curtain is "OPEN" (curtains on the side of the window) or "CLOSED" (curtains in front of the window)


### Temperature control panel ###

The temperature control panel allows to control individual heating components.

The control panel is composed of:

- mode selection buttons allowing to choose between "DAY" (warm mode), "NIGHT" (eco mode) or "OFF" (disabled)
- a pictogram indicating if the heater is in "DAY" mode (red), "NIGHT" mode (blue) or "OFF" (black), and the current room temperature
- a "-"/"+" temperature adjuster with the desired value


Development
-----------

### Dev Stack

We assume the following development stack in the documentation:

- [nodejs](http://nodejs.org/)
- npm: the node.js package manager (bundled with node)
- [Bower]: the front-end libraries package manager
- [Grunt]: the JavaScript task runner


### Installation

To install the application, just clone the repository and then issue the following commands in your terminal:

    npm install
    bower install
    grunt serve

The application will be available at the following url: `http://localhost:3000/`


### Project structure

- `app`: the raw application code
    - `assets`: contains all the application assets
        - `fonts`: icon fonts
        - `scripts`: CoffeeScript files
            - `panels`: GUI control panels for the components
            - `server`: server emulation files
            - `domotix.coffee`: main client file
        - `styles`:  SASS files
        - `vendor`: vendor libraries
    - `index.html`: main application file
- `dist`: the compiled and optimized application code
    - …
    - `index.html`: main application file (the one to use to test the application)
- `.bowerrc`: [Bower] configuration file
- `bower.json`: [Bower] packages used by the application
- `Gruntfile.coffee`: [Grunt] build script
- `LICENCE`: MIT Licence file
- `package.json`: Application description file
- `README.md`: Markdown readme file (this file actually)


### Extending the app

#### Adding components

In order to add new components to the application, you need to:

1. Define a shim component in `app/assets/scripts/server/components` for the new component, and make sure it extends the default component (`app/assets/scripts/server/components/component.coffee`)
2. Add some instance of the new component at the end of `app/assets/scripts/server/server.coffee`
3. Create the control panel for the new component in `app/assets/scripts/panels`, and make sure it extends the default panel (`app/assets/scripts/panels/panel.coffee`)
4. If required, also add a general controller for the new component type in the `app/index.html` file and handle its events at the end of `app/assets/scripts/domotix.coffee`

#### Adding rooms

In order to add new rooms to the application, you need to:

1. Register a new room in `app/assets/scripts/server/server.coffee`
2. Add the components of the room in the same file


3rd-party libraries
-------------------

- [jQuery](http://jquery.com/) as base JavaScript library
- [normalize.css](http://necolas.github.io/normalize.css/) for CSS normalization across browsers
- [Bootstrap] as front-end framework for quicker prototyping
- Subset of [Entypo Pictogram Suite](http://www.entypo.com/) to enhance the GUI
- [RequireJS](http://requirejs.org/) and [almond](https://github.com/jrburke/almond) to handle JavaScript modules dependencies and loading

[Bootstrap]: http://getbootstrap.com/
[Bower]: http://bower.io/
[Grunt]: http://gruntjs.com/


Release History
---------------

- `1.0.6` Fix jQuery vendor library path
- `1.0.5` Updated documentation
- `1.0.4` Updates to light and temp controls, navbar collapse fix
- `1.0.3` Small screen CSS fix
- `1.0.2` Code optimizations
- `1.0.1` Cross-browser CSS fixes
- `1.0.0` Initial release


License
-------

Copyright (c) 2014 Julien Ramboz Licensed under the MIT license.
