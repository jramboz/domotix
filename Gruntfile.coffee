module.exports = (grunt) ->

  # Configuring main directories
  dev_dir     = "app"
  dist_dir    = "dist"
  pkg     = grunt.file.readJSON "package.json"
  
  # Project configuration.
  grunt.initConfig
    pkg: pkg

    # Clean up output folders
    clean:
      dist: dist_dir

    # Copy static files to output folders
    copy:
      vendor:
        files: [
          expand: true
          cwd: "#{dev_dir}/assets/vendor/"
          src: [
            "**/*"
            "!**/{test*,less}/**"
            "!**/{README.md,*.json,*.map}"
          ]
          dest: "#{dist_dir}/assets/vendor/"
        ]
      html:
        files: [
          expand: true
          cwd: dev_dir
          src: [
            "**/*.html"
          ]
          dest: dist_dir
        ]
      fonts:
        files: [
          expand: true
          cwd: "#{dev_dir}/assets/fonts/"
          src: [
            "**/*"
          ]
          dest: "#{dist_dir}/assets/fonts/"
        ]

    # Compile CoffeeScript files to JavaScript
    coffee:
      compile:
        files: [
          expand: true
          cwd: "#{dev_dir}/assets/scripts"
          src: ["**/*.coffee"]
          dest: "#{dist_dir}/assets/scripts"
          ext: ".js"
        ]

    # Compile SASS files to CSS
    sass:
      compile:
        files: [
          expand: true
          cwd: "#{dev_dir}/assets/styles"
          src: ["*.sass"]
          dest: "#{dist_dir}/assets/styles"
          ext: ".css"
        ]

    # JS Linting
    jshint:
      lint:
        src: ["#{dist_dir}/assets/scripts/*.js", "!**/*-built.js", "!**/*.min.js"]

    # CSS Linting
    csslint:
      lint:
        src: ["#{dist_dir}/assets/styles/*.css", "!**/*.min.css"]
        options:
          "adjoining-classes": false
          "box-model": false
          "unqualified-attributes": false

    # Optimize require modules using almond
    requirejs:
      options:
        include: "../vendor/almond/almond.js"
        baseUrl: "#{dist_dir}/assets/scripts"
        optimize: "none"
      optimize:
        options:
          name: pkg.name
          mainConfigFile: "#{dist_dir}/assets/scripts/domotix.js"
          out: "#{dist_dir}/assets/scripts/domotix-built.js"

    # Minify generated JavaScript files
    uglify:
      options:
        preserveComments: false
      app:
        files: [
          expand: true
          cwd: "#{dist_dir}/assets/"
          src: ["scripts/domotix-built.js", "!**/*.min.js"]
          dest: "#{dist_dir}/assets/"
          ext: ".min.js"
        ]
      vendor:
        files: [
          expand: true
          cwd: "#{dist_dir}/assets/"
          src: ["vendor/**/*.js", "!**/*.min.js", "!**/src/*.js"]
          dest: "#{dist_dir}/assets/"
          ext: ".min.js"
        ]

    # Minify generated CSS files
    cssmin:
      options:
        keepSpecialComments: 0
      minify:
        files: [
          expand: true
          cwd: "#{dist_dir}/assets/"
          src: ["**/*.css", "!**/*.min.css"]
          dest: "#{dist_dir}/assets/"
          ext: ".min.css"
        ]
    
    # Validate pages
    validation:
      options:
        reportpath: false
      validate:
        files:
          "validation": "#{dist_dir}/**/*.html"

    # Check accessibility
    accessibility:
      options:
        accessibilityLevel: "WCAG2A"
      validate:
        files: [
          expand: true
          cwd: dist_dir
          src: ["*.html"]
          dest: "./dist"
          ext: "-report.txt"
        ]

    # Watch files for changes and execute corresponding tasks (livereload)
    watch:
      options:
        livereload: true
        files: ["#{dist_dir}/**/*"]
      scripts:
        files: ["Gruntfile.coffee", "#{dev_dir}/**/*.coffee"]
        tasks: ["newer:coffee:compile", "newer:jshint:lint", "requirejs:optimize", "uglify:app"]
      styles:
        files: ["#{dev_dir}/**/*.sass"]
        tasks: ["newer:sass:compile", "newer:csslint:lint", "newer:cssmin:minify"]
      html:
        files: ["#{dev_dir}/**/*.html"]
        #tasks: ["newer:validation:validate", "newer:accessibility:validate"]
        tasks: ["newer:copy:html", "newer:validation:validate"]

    # Serve up generated files
    connect:
      serve:
        options:
          port: 3000
          base: dist_dir
          livereload: true


  # Load tasks
  require("load-grunt-tasks") grunt

  # Default build task
  grunt.registerTask "default", [
    "clean:dist"
    "copy"
    "coffee:compile"
    "sass:compile"
    "jshint:lint"
    "csslint:lint"
    "requirejs:optimize"
    "uglify"
    "cssmin:minify"
    "validation:validate"
    #"accessibility:validate"
  ]

  # Build the code
  grunt.registerTask "build", [
    "clean:dist"
    "copy"
    "coffee:compile"
    "sass:compile"
    "jshint:lint"
    "csslint:lint"
    "requirejs:optimize"
    "uglify"
    "cssmin:minify"
  ]

  # Serve and watch the code
  grunt.registerTask "serve", [
    "build"
    "connect:serve"
    "watch"
  ]
